package com.bowen.servicefeign.clients.fallback;

import com.bowen.servicefeign.clients.SchedualServiceHi;
import org.springframework.stereotype.Component;


@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {

	@Override
	public String sayHiFromClientOne(String name) {
		return "sorry "+name;
	}
}
