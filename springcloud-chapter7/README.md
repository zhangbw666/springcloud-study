# SpringCloud 实战教程 | 007篇: 高可用的分布式配置中心SpringCloud Config

上一篇文章讲述了一个服务如何从配置中心读取文件，配置中心如何从远程git读取配置文件，当服务实例很多时，都从配置中心读取文件，这时可以考虑将配置中心做成一个微服务，将其集群化，从而达到高可用，架构图如下：

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/07/springcloud7-1.jpg)  

## 1. 准备工作

继续使用上一篇文章的工程，创建一个eureka-server工程，用作服务注册中心。

在其pom.xml文件引入Eureka的起步依赖spring-cloud-starter-eureka-server，代码如下:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.bowen</groupId>
    <artifactId>eureka-server</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>eureka-server</name>
    <description>Demo project for Spring Boot</description>

    <parent>
        <groupId>com.bowen</groupId>
        <artifactId>springcloud-chapter7</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>

    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
        </dependency>
    </dependencies>

</project>
```

在配置文件application.yml上，指定服务端口为8889，加上作为服务注册中心的基本配置，代码如下：

```xml
server:
  port: 8889

eureka:
  instance:
    hostname: localhost
  client:
    registerWithEureka: false
    fetchRegistry: false
    serviceUrl:
      defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/

spring:
  application:
    name: eurka-server
```

入口类：

```java
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run( EurekaServerApplication.class, args );
    }
}
```

## 2. 改造config-server

在其pom.xml文件加上EurekaClient的起步依赖spring-cloud-starter-eureka，代码如下:

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-config-server</artifactId>
    </dependency>
</dependencies>
```

配置文件application.yml，指定服务注册地址为http://localhost:8889/eureka/，其他配置同上一篇文章，完整的配置如下：

```xml
spring.application.name=config-server
server.port=8888

spring.cloud.config.server.git.uri=https://github.com/bestsoftman/springcloud-study/
spring.cloud.config.server.git.searchPaths=respo
spring.cloud.config.label=master
spring.cloud.config.server.git.username=478231309@qq.com
spring.cloud.config.server.git.password=chbqj1205
eureka.client.serviceUrl.defaultZone=http://localhost:8889/eureka/
```

最后需要在程序的启动类Application加上@EnableEureka的注解。

## 3. 改造config-client

将其注册到服务注册中心，作为Eureka客户端，需要pom文件加上起步依赖spring-cloud-starter-eureka，代码如下：

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-config</artifactId>
    </dependency>
</dependencies>
```

配置文件bootstrap.properties，注意是bootstrap。加上服务注册地址为http://localhost:8889/eureka/

```xml
spring.application.name=config-client
spring.cloud.config.label=master
spring.cloud.config.profile=dev
# spring.cloud.config.uri= http://localhost:8888/
server.port=8881

eureka.client.serviceUrl.defaultZone=http://localhost:8889/eureka/
spring.cloud.config.discovery.enabled=true
spring.cloud.config.discovery.serviceId=config-server
```
- spring.cloud.config.discovery.enabled 是从配置中心读取文件。
- spring.cloud.config.discovery.serviceId 配置中心的servieId，即服务名。

这时发现，在读取配置文件不再写ip地址，而是服务名，这时如果配置服务部署多份，通过负载均衡，从而高可用。

依次启动eureka-server,config-server,config-client 访问网址：http://localhost:8889/

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/07/springcloud7-2.jpg)  

访问http://localhost:8881/hi，浏览器显示：

>boo version 20

## 4. 总结

本节主要讲解了高可用的分布式配置中心SpringCloud Config，通过本节的学习，可以进一步加深对SpringCloud配置中心的理解。

源码下载地址：[点击下载](https://gitee.com/zhangbw666/springcloud-study/tree/master/springcloud-chapter7)

> 欢迎关注微信公众号：猿码天地  

![公众号【猿码天地】](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)  