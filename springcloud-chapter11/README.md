# SpringCloud 实战教程 | 011篇: 断路器监控Hystrix Dashboard

在我的 [第四篇文章](https://gitee.com/zhangbw666/springcloud-study/tree/master/springcloud-chapter4)断路器讲述了如何使用断路器，并简单的介绍了下Hystrix Dashboard组件，这篇文章更加详细的介绍Hystrix Dashboard。

## 1. Hystrix Dashboard简介

在微服务架构中为了保证程序的可用性，防止程序出错导致网络阻塞，出现了断路器模型。断路器的状况反应了一个程序的可用性和健壮性，它是一个重要指标。Hystrix Dashboard是作为断路器状态的一个组件，提供了数据监控和友好的图形化界面。

## 2. 准备工作

本文的的工程栗子，来源于第一篇文章的栗子，在它的基础上进行改造。

## 3. 改造service-hi

在pom的工程文件引入相应的依赖：

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
    </dependency>
</dependencies>
```

在程序的入口ServiceHiApplication类，加上@EnableHystrix注解开启断路器，这个是必须的，并且需要在程序中声明断路点HystrixCommand；加上@EnableHystrixDashboard注解，开启HystrixDashboard

```java
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@RestController
@EnableHystrix
@EnableHystrixDashboard
@EnableCircuitBreaker
public class ServiceHiApplication {

	/**
	 * 访问地址 http://localhost:8762/actuator/hystrix.stream
	 * @param args
	 */

	public static void main(String[] args) {
		SpringApplication.run( ServiceHiApplication.class, args );
	}

	@Value("${server.port}")
	String port;

	@RequestMapping("/hi")
	@HystrixCommand(fallbackMethod = "hiError")
	public String home(@RequestParam(value = "name", defaultValue = "bowen") String name) {
		return "hi " + name + " ,i am from port:" + port;
	}

	public String hiError(String name) {
		return "hi,"+name+",sorry,error!";
	}

}
```

运行程序： 依次开启eureka-server 和service-hi.

## 4. Hystrix Dashboard图形展示

打开http://localhost:8762/actuator/hystrix.stream，可以看到一些具体的数据：

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/11/springcloud11-1.jpg)  

打开localhost:8762/hystrix 可以看见以下界面：

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/11/springcloud11-2.jpg)  

在界面依次输入：http://localhost:8762/actuator/hystrix.stream 、2000 、bowen ；点确定。

在另一个窗口输入： http://localhost:8762/hi?name=bowen

重新刷新hystrix.stream网页，你会看到良好的图形化界面：

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/11/springcloud11-3.jpg)  

## 5. 总结

本节文章主要讲解了断路器监控Hystrix Dashboard，并通过源码的方式进行了实践。通过本节的学习，读者可快速掌握断路器监控Hystrix Dashboard的使用。

源码下载地址：[点击下载](https://gitee.com/zhangbw666/springcloud-study/tree/master/springcloud-chapter11)

> 欢迎关注微信公众号：猿码天地  

![公众号【猿码天地】](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)  