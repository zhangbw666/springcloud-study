# SpringCloud 实战教程 | 008篇: 消息总线SpringCloud Bus

Spring Cloud Bus 将分布式的节点用轻量的消息代理连接起来。它可以用于广播配置文件的更改或者服务之间的通讯，也可以用于监控。本文要讲述的是用Spring Cloud Bus实现通知微服务架构的配置文件的更改。

## 1. 准备工作

本文还是基于上一篇文章来实现。按照官方文档，我们只需要在配置文件中配置 spring-cloud-starter-bus-amqp；这就是说我们需要装rabbitMq。至于怎么使用rabbitmq，搜索引擎下。

## 2. 改造config-client

在pom文件加上起步依赖spring-cloud-starter-bus-amqp，完整的配置文件如下：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.bowen</groupId>
	<artifactId>config-client</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>config-client</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>com.bowen</groupId>
		<artifactId>springcloud-chapter8</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>

	<dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bus-amqp</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
```
在配置文件application.properties中加上RabbitMq的配置，包括RabbitMq的地址、端口，用户名、密码，代码如下：

```xml
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest
```

如果rabbitmq有用户名密码，输入即可。

依次启动eureka-server、confg-cserver,启动两个config-client，端口为：8881、8882。

访问http://localhost:8881/hi 或者http://localhost:8882/hi 浏览器显示：

>boo version 20

这时我们去代码仓库将boo的值改为“boo version 21”，即改变配置文件boo的值。如果是传统的做法，需要重启服务，才能达到配置文件的更新。此时，我们只需要发送post请求：http://localhost:8881/bus/refresh，你会发现config-client会重先去拿配置文件

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/08/springcloud8-1.jpg)  

重新读取配置文件：

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/08/springcloud8-2.jpg)  

这时我们再访问http://localhost:8881/hi 或者http://localhost:8882/hi 浏览器显示：

>boo version 21

另外，/bus/refresh接口可以指定服务，即使用”destination”参数，比如 “/bus/refresh?destination=customers:**” 即刷新服务名为customers的所有服务，不管ip。

## 3. 分析

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/08/springcloud8-3.jpg)  

当git文件更改的时候，通过pc端用post 向端口为8882的config-client发送请求/bus/refresh/；此时8882端口会发送一个消息，由消息总线向其他服务传递，从而使整个微服务集群都达到更新配置文件。

## 4. 总结

本节主要讲解了消息总线SpringCloud Bus，Spring Cloud Bus 将分布式的节点用轻量的消息代理连接起来。它可以用于广播配置文件的更改或者服务之间的通讯，也可以用于监控。通过本节的学习，读者对消息总线的应用会有一个深刻的了解。

源码下载地址：[点击下载](https://gitee.com/zhangbw666/springcloud-study/tree/master/springcloud-chapter8)

> 欢迎关注微信公众号：猿码天地  

![公众号【猿码天地】](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)  