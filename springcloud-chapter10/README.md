# SpringCloud 实战教程 | 010篇: 高可用的服务注册中心

## 1. 准备工作

Eureka通过运行多个实例，使其更具有高可用性。事实上，这是它默认的属性，你需要做的就是给对等的实例一个合法的关联serviceurl。

这篇文章我们基于[第一篇文章](https://gitee.com/zhangbw666/springcloud-study/tree/master/springcloud-chapter1)的工程，来做修改。

## 2. 改造工作

在eureka-server工程中resources文件夹下，修改application.yml配置文件，完整文件如下：  

```xml

---
spring:
   profiles: peer1
server:
   port: 8761
eureka:
   instance:
      hostname: peer1
   client:
      serviceUrl:
         defaultZone: http://peer2:8769/eureka/

---
spring:
   profiles: peer2
server:
   port: 8769
eureka:
   instance:
      hostname: peer2
   client:
      serviceUrl:
         defaultZone: http://peer1:8761/eureka/
         
```

按照官方文档的指示，需要改变etc/hosts，linux系统通过vim /etc/hosts ,加上：

>127.0.0.1 peer1  
>127.0.0.1 peer2

windows电脑，在c:/windows/systems/drivers/etc/hosts 修改。

这时需要改造下service-hi:

```xml
eureka:
  client:
    serviceUrl:
      defaultZone: http://peer1:8761/eureka/

server:
  port: 8762

spring:
  application:
    name: service-hi
```

## 3. 启动工程

启动eureka-server：

>java -jar eureka-server-0.0.1-SNAPSHOT.jar - -spring.profiles.active=peer1

>java -jar eureka-server-0.0.1-SNAPSHOT.jar - -spring.profiles.active=peer2

启动service-hi:

> java -jar service-hi-0.0.1-SNAPSHOT.jar

访问：localhost:8761,如图：

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/10/springcloud10-1.jpg)  

你会发现注册了service-hi，并且有个peer2节点，同理访问localhost:8769你会发现有个peer1节点。

client只向8761注册，但是你打开8769，你也会发现，8769也有 client的注册信息。

个人感受：这是通过看官方文档的写的demo ，但是需要手动改host是不是不符合Spring Cloud 的高上大？

此时的架构图：

![](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-springcloud-study/10/springcloud10-2.jpg)  

Eureka-eserver peer1 8761,Eureka-eserver peer2 8769相互感应，当有服务注册时，两个Eureka-eserver是对等的，它们都存有相同的信息，这就是通过服务器的冗余来增加可靠性，当有一台服务器宕机了，服务并不会终止，因为另一台服务存有相同的数据。

## 4. 总结

本节文章主要讲解了高可用的服务注册中心，通过本节的学习，读者可加深对注册中心的理解，并能在实际工作中应用。  

源码下载地址：[点击下载](https://gitee.com/zhangbw666/springcloud-study/tree/master/springcloud-chapter10)

> 欢迎关注微信公众号：猿码天地  

![公众号【猿码天地】](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)  