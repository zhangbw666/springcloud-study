# SpringCloud 实战教程

当今世界，互联网飞速发展，并融入生活的方方面面，人们对互联网产品提出了更严格的要求，重体验、重响应速度、关联性强、业务场景越来越复杂等是互联网产品的新特点。这对软件架构提出了新的要求，如何使得系统架构能够轻松持续改进、快速部署、业务之间低耦合，成为软件架构的新的思考方向。

微服务作为系统解决这些问题的一种思路应运而生，方兴未艾。

我们通过以下几个问题来开启微服务之旅：

- 什么是微服务？  
- 微服务与传统软件架构的对比？  
- 为什么要使用微服务？  
- 微服务的设计原则和具体特点？  
- 微服务应该具备的功能有哪些？  
- 微服务应用场景？
- 微服务需要使用的组件有哪些？
- 如何综合运用这些组件构建整个微服务框架？

带着这些问题，我们正式开启微服务之旅！

本系列文章会介绍服务注册与发现 Eureka 组件、服务消费（Rest+Ribbon、Feign两种方式）、负载均衡 Ribbon 组件、熔断器 Hystrix 组件、路由网关 Zuul 组件、分布式配置中心SpringCloud Config、消息总线SpringCloud Bus、服务链路追踪SpringCloud Sleuth、断路器监控Hystrix Dashboard、断路器聚合监控Hystrix Turbine等技术组件！

- SpringCloud 实战教程 | 001篇: 服务注册与发现Eureka
- SpringCloud 实战教程 | 002篇: 服务消费者Rest+Ribbon方式
- SpringCloud 实战教程 | 003篇: 服务消费者Feign方式
- SpringCloud 实战教程 | 004篇: 断路器Hystrix
- SpringCloud 实战教程 | 005篇: 路由网关Zuul
- SpringCloud 实战教程 | 006篇: 分布式配置中心SpringCloud Config
- SpringCloud 实战教程 | 007篇: 高可用的分布式配置中心SpringCloud Config
- SpringCloud 实战教程 | 008篇: 消息总线SpringCloud Bus
- SpringCloud 实战教程 | 009篇: 服务链路追踪SpringCloud Sleuth
- SpringCloud 实战教程 | 010篇: 高可用的服务注册中心
- SpringCloud 实战教程 | 011篇: 断路器监控Hystrix Dashboard
- SpringCloud 实战教程 | 012篇: 断路器聚合监控Hystrix Turbine

本人从事互联网开发，分享java开发中常用的技术，分享软件开发中各种新技术的应用方法，不定期分享推送java技术相关或者互联网相关文章。

1、微信公众号【猿码天地】  
![猿码天地](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)  

2、CSDN博客【猿码天地】  
我会陆续写一些关于互联网技术方面的文章，感兴趣的朋友可以关注我，相信你一定会有所收获。如果有java等开发方面的问题，或者是IT求职方面的问题，都可以私信我。  
